import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private httpClient: HttpClient) { }


  getQuestionsExceuter() {

    return this.httpClient.get('../../assets/questions.json').pipe(take(1), map((questions) => { return questions }));


  }



}
