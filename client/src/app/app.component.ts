import { Component, OnInit } from '@angular/core';
import { QuestionsService } from './clientservices/questions.service';
import { IQuestion } from './question.interface';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  currentQuestion = 0;
  totalQuestions: number;
  modalHeading: string = "Answer";
  correctAnswerModalSubhead: string = "Congratulations !!! Correct Answer";
  incorrectAnswerModalSubhead: string = "Incorrect Answer";
  modalSubheading: string = "";
  correctAnswers: number = 0;
  incorrectAnswers: number = 0;
  scoresSelected: boolean = false;
  startAgain: boolean = false;

  questions: IQuestion[];



  constructor(private questionsService: QuestionsService, private snackBar: MatSnackBar) { }
  selectedvalue: any;
  options: any;

  ngOnInit() {
    this.questionsService.getQuestionsExceuter().subscribe((res: any) => {
      console.log(res);
      this.questions = res.questions;
      this.totalQuestions = this.questions.length;
    })
  }






  handleChange(question, quesNo, optionSelected, optionName, correctAnswer, basicModal) {
    console.log((quesNo + 1) + "." + question + "?");
    console.log("Answer Selected -> " + "option Number - " + optionSelected + " and option Name - " + optionName);
    if (optionSelected == correctAnswer.key) {
      this.modalSubheading = this.correctAnswerModalSubhead;
      this.correctAnswers = this.correctAnswers + 1;
      basicModal.show();
    }
    else {
      this.modalSubheading = this.incorrectAnswerModalSubhead;
      this.incorrectAnswers = this.incorrectAnswers + 1;
      basicModal.show();

    }




  }


  onNextQuestion() {
    if ((this.currentQuestion + 1) == this.totalQuestions) {
      console.log("you have completed the test .thanks");
      this.currentQuestion = this.currentQuestion + 1;

    }
    else {
      this.currentQuestion = this.currentQuestion + 1;
    }
  }

  onbackQuestion() {
    this.currentQuestion = this.currentQuestion - 1;

  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  checkScore() {
    this.scoresSelected = !this.scoresSelected;
    console.log(this.currentQuestion, this.totalQuestions);
  }


  tryAgain() {
    this.currentQuestion = 0;
    this.scoresSelected = !this.scoresSelected;
    this.correctAnswers = 0;
    this.incorrectAnswers = 0;
  }
}
