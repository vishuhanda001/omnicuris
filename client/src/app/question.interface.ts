export interface IQuestion {

    ques: string,
    ans: Ioption[],
    correctanswer: Icorrectanswer

}


interface Ioption {

    option: string,
    key: number



}


interface Icorrectanswer {

    option: string,
    key: number

}